﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour
{

	public Text Player1ScoreUI;
	public Text Player2ScoreUI;
	public Text Timer;
	public GameObject Mainmenu;
	public static int Player1Score;
	public static int Player2Score;
	public static int Player1Hunger;
	public static int Player2Hunger;
	public static int TimerTime = 120;
	private static ScoreManager Instance;
	private static Coroutine timerRoutine;

	private void Start()
	{
		Instance = this;
		Reset();
	}

	void Update()
	{
		Player1ScoreUI.text = Player1Score.ToString();
		Player2ScoreUI.text = Player2Score.ToString();
		if (TimerTime < 0)
			TimerTime = 0;
		Timer.text = TimerTime.ToString();
	}

	public static void StopTimer()
	{
		if (timerRoutine != null)
			Instance.StopCoroutine(timerRoutine);
		timerRoutine = null;
	}

	public static void StartTimer()
	{
		StopTimer();
		timerRoutine = Instance.StartCoroutine(Instance.HandleTimer());
	}

	private IEnumerator HandleTimer()
	{
		while (TimerTime >= 0)
		{
			yield return new WaitForSeconds(1);
			TimerTime -= 1;
		}
		Mainmenu.SetActive(true);
	}

	public static void Reset(int timer = 60, int p1score = 0, int p2score = 0)
	{
		Player1Score = p1score;
		Player2Score = p2score;
		TimerTime = timer;
	}
}
