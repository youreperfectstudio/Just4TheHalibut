﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using UnityEngine;

public class LineController : MonoBehaviour
{

	public float MaxScale = 2f;
	public float MinScale = 0.3f;
	public float Speed = 0.1f;
	public float SpeedMultiplier = 2f;

	private Vector3 v3OrgPos;
	private float orgScale;
	private float endScale;

	public bool HumanControlled = true;
	public float value;
	public string AxisName = "Vertical P1";

	void Start()
	{
		v3OrgPos = transform.position;
		orgScale = transform.localScale.z;
		endScale = orgScale;
	}

	void Update()
	{

		if (HumanControlled)
			value = CnControls.CnInputManager.GetAxis(AxisName);

		float currentSpeed = Speed * SpeedMultiplier;

		if (value < 0)
		{
			if (transform.localScale.x < MaxScale)
			{
				Vector3 newScale = new Vector3(transform.localScale.x + currentSpeed, transform.localScale.y, transform.localScale.z);
				transform.localScale = Vector3.Lerp(transform.localScale, newScale, Time.deltaTime * 2);
			}
		}
		else if (value > 0)
		{
			if (transform.localScale.x > MinScale)
			{
				Vector3 newScale = new Vector3(transform.localScale.x - currentSpeed, transform.localScale.y, transform.localScale.z);
				transform.localScale = Vector3.Lerp(transform.localScale, newScale, Time.deltaTime * 2);
			}
		}
	}
}