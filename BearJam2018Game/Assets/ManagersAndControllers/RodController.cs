﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using UnityEngine;
using UnityEngine.UI;

public class RodController : MonoBehaviour
{
	public bool HumanControlled = true;
	public float value;
	public string AxisName = "Horizontal P1";
	public bool Invert = false;
	public float Speed = 1f;
	public float MinScale = 0.9f;
	public float MaxScale = 0.18f;

	public bool TouchControls = false;
	Button LeftButton;

	public void OnLeftPressed()
	{
		TouchControls = true;
		value = -1;
	}

	public void OnLeftReleased()
	{
		TouchControls = true;
		value = -1;
	}

	void Update()
	{
		if (HumanControlled)
			value = CnControls.CnInputManager.GetAxis(AxisName);

		if (Invert)
			value = -value;

		if (value > 0)
		{
			if (transform.localScale.x < MinScale)
			{
				Vector3 newScale = new Vector3(transform.localScale.x + Mathf.Abs(Speed*value), transform.localScale.y, transform.localScale.z);
				transform.localScale = Vector3.Lerp(transform.localScale, newScale, Time.deltaTime * 2);
			}
		}
		else if (value < 0)
		{
			if (transform.localScale.x > MaxScale)
			{
				Vector3 newScale = new Vector3(transform.localScale.x - Mathf.Abs(Speed*value), transform.localScale.y, transform.localScale.z);
				transform.localScale = Vector3.Lerp(transform.localScale, newScale, Time.deltaTime * 2);
			}
		}
	}
}
