﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using System.Collections;
using UnityEngine;

public class SoundManager : MonoBehaviour
{

	public AudioClip ScoreSound;
	public AudioClip BigScoreSound;
	public AudioClip ReelSound;
	public AudioClip WhaleSound;
	public AudioClip SplashSound;
	public AudioClip ExitSound;

	//These need to be moved to some other class
	public Animator LeftBear;
	public Animator RightBear;
	public MainMenu MainMenu;
	internal static bool Player2AI = true;

	internal static SoundManager Instance;

	private void Start()
	{
		Instance = this;
	}

	public static void PlayScore()
	{
		Instance.GetComponent<AudioSource>().PlayOneShot(Instance.ScoreSound);
	}

	public static void PlayBigScore()
	{
		Instance.GetComponent<AudioSource>().PlayOneShot(Instance.BigScoreSound);
	}

	public static void PlayWhale()
	{
		Instance.GetComponent<AudioSource>().PlayOneShot(Instance.WhaleSound);
	}

	private IEnumerator PlayExitMusic()
	{
		Instance.GetComponent<AudioSource>().Stop();
		Instance.GetComponent<AudioSource>().clip = ExitSound;
		Instance.GetComponent<AudioSource>().Play();
		yield return new WaitForSeconds(3);
		Application.Quit();
	}

	/// <summary>
	/// This doesn't belong here but I stuck it here as a quick fix since we know SoundManager is in every scene.
	/// </summary>
	public void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.Q))
		{
			if (MainMenu.gameObject != null && MainMenu.gameObject.activeInHierarchy)
			{
#if UNITY_EDITOR
				UnityEditor.EditorApplication.isPlaying = false;
#else
				StartCoroutine(PlayExitMusic());
#endif
			}
			else
			{
				MainMenu.gameObject.SetActive(true);
			}
		}
	}

}
