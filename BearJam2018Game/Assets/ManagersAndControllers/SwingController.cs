﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using UnityEngine;

public class SwingController : MonoBehaviour
{
	public HingeJoint2D hinge;
	public float angle;
	public float limits;
	public float torque = 25;
	public float speed = 200;
	public bool hooked = false;
	public float duration = 1;    //the max time of a move session
	float elapsedTime = 0f; //time since started moving
	float wait = 0f; //wait this much time
	float waitTime = 0f;
	public bool MirrorLimits = false;

	public GameObject hookedFish;

	void Start()
	{
		hinge = GetComponent<HingeJoint2D>();
		limitSwing();
	}

	void limitSwing()
	{
		JointAngleLimits2D jl = new JointAngleLimits2D();
		if (!MirrorLimits)
		{
			jl.max = 90;
			jl.min = 90;
		}
		else
		{
			jl.max = -90;
			jl.min = -90;
		}
		hinge.limits = jl;
	}

	void unlimitSwing()
	{
		JointAngleLimits2D jl = new JointAngleLimits2D();
		if (!MirrorLimits)
		{
			jl.max = 120;
			jl.min = 60;
		}
		else
		{
			jl.max = -120; //-145?
			jl.min = -60; //145?
		}
		hinge.limits = jl;
	}

	void Update()
	{
		angle = hinge.jointAngle % 360;
		JointMotor2D motor = hinge.motor;
		motor.maxMotorTorque = torque;
		motor.motorSpeed = speed;
		hinge.motor = motor;

		if (hooked)
		{
			if (hookedFish.transform.localScale.y == 180)
			{
				speed *= -1;
			}
			unlimitSwing();
			if (elapsedTime < duration)
			{
				hinge.useMotor = true;
				elapsedTime += Time.deltaTime;
			}
			else
			{
				hookedFish.transform.Rotate(0, 180, 0);
				motor.motorSpeed *= -1;
				speed = motor.motorSpeed;
				elapsedTime = 0;
			}
		}
		else
		{
			hinge.useMotor = false;
			limitSwing();
		}
	}

}
