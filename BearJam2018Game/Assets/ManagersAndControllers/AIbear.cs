﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using UnityEngine;

public class AIbear : MonoBehaviour
{
	public GameObject target;
	public Vector2 size;
	public float angle = 0f;
	public LayerMask fishlayer;
	LineController vert;
	RodController horiz;
	public bool hooked = false;

	void SearchFish(Vector2 center, Vector2 boxsize, float angle)
	{
		BaseFish[] fishList = GameObject.FindObjectsOfType<BaseFish>();
		if (fishList.Length == 0)
		{
			Debug.Log("NoFish");
			target = null;
		}
		else
		{
			target = fishList[Random.Range(0, fishList.Length - 1)].gameObject;
		}
		if (target.transform.position.x < 0)
			target = null;
	}
	void Start()
	{
		GameObject o1 = GameObject.Find("line2");
		vert = o1.GetComponent<LineController>();
		GameObject o2 = GameObject.Find("RodPoint2");
		horiz = o2.GetComponent<RodController>();
	}

	public float targetTimeout = 5;
	void FixedUpdate()
	{

		if (!SoundManager.Player2AI)
			return;

		if (!hooked)
		{
			if (target != null)
			{

				if (target.transform.position.x > transform.position.x + 2)
				{
					horiz.value = 1;
				}
				else if (target.transform.position.x < transform.position.x - 2)
				{
					horiz.value = -1;
				}
				else
				{
					horiz.value = 0;
				}

				if (target.transform.position.y > transform.position.y)
				{
					vert.value = 1;
				}
				else if (target.transform.position.y < transform.position.y)
				{
					vert.value = -1;
				}
				else
				{
					vert.value = 0;
				}

				targetTimeout -= Time.deltaTime;
				if (targetTimeout < 0)
				{
					targetTimeout = 5;
					target = null;
				}
			}
			else
			{
				targetTimeout = 5;
				vert.value = -0.1f;
				horiz.value = -0.1f;
				SearchFish(new Vector2(transform.position.x, transform.position.y), size, angle);
			}
		}
		else
		{
			vert.value = 1;
		}
	}
}
