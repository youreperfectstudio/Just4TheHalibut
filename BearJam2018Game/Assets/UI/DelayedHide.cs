﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DelayedHide : MonoBehaviour
{

	public void Hide(float seconds)
	{
		if(gameObject.activeInHierarchy)
			StartCoroutine(HideHelper(seconds));
	}

	private IEnumerator HideHelper(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		gameObject.SetActive(false);
	}
}
