﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using CnControls;
using GameJolt.API;
using GameJolt.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

	public AudioClip MainMenuMusic;
	public AudioClip GameMusic;
	public AudioClip SelectSound;
	public AudioSource MusicPlayer;
	public GameObject InfoBar;
	public GameObject KeyboardControlsInfo;
	public Text Player1Winner;
	public Text Player2Winner;
	public Sprite RobotIcon;
	public Sprite HumanIcon;
	public Sprite NoneIcon;
	public Button Player2Button;
	public Button Player2TouchControls;
	public Text Player2KeyboardControls;
	public SimpleJoystick Player2Stick;
	public LineController Player2Line;
	public RodController Player2Rod;

	public HighScorePanel HighScorePanel;

	public Image SpashScreen;

	public void Start()
	{

	}

	public IEnumerator OnHighScoreHelper(float delay)
	{
		yield return new WaitForSeconds(delay);
		if (Application.internetReachability != NetworkReachability.NotReachable)
			GameJoltUI.Instance.ShowLeaderboards();
	}

	void OnEnable()
	{
		KeyboardControlsInfo.SetActive(true);
		if (ScoreManager.Player1Score >= ScoreManager.Player2Score && ScoreManager.Player1Score != 0)
			Player1Winner.gameObject.SetActive(true);
		else if (ScoreManager.Player2Score > ScoreManager.Player1Score)
			Player2Winner.gameObject.SetActive(true);
		if (ScoreManager.Player1Score == ScoreManager.Player2Score && ScoreManager.Player1Score != 0)
			Player2Winner.gameObject.SetActive(true);

		int oldScore = PlayerPrefs.GetInt("HighScore", 0);

		if(Player1Winner.gameObject.activeInHierarchy && ScoreManager.Player1Score > oldScore)
		{
			if (GameJoltAPI.Instance.HasSignedInUser)
			{
				if(Application.internetReachability != NetworkReachability.NotReachable)
					GameJolt.API.Scores.Add(ScoreManager.Player1Score, ScoreManager.Player1Score+" Points");
				PlayerPrefs.SetInt("HighScore", ScoreManager.Player1Score);
				PlayerPrefs.Save();
				StartCoroutine(OnHighScoreHelper(1f));
			}
			else
			{
				PlayerPrefs.SetInt("HighScore", ScoreManager.Player1Score);
				PlayerPrefs.Save();
				if (Application.internetReachability != NetworkReachability.NotReachable)
					HighScorePanel.gameObject.SetActive(true);
			}
		}
		else if(Player2Winner.gameObject.activeInHierarchy && !SoundManager.Player2AI && ScoreManager.Player2Score > oldScore)
		{
			PlayerPrefs.SetInt("HighScore", ScoreManager.Player2Score);
			PlayerPrefs.Save();
			if (Application.internetReachability != NetworkReachability.NotReachable)
				HighScorePanel.gameObject.SetActive(true);
		}

		ScoreManager.StopTimer();
		MusicPlayer.Stop();
		MusicPlayer.clip = MainMenuMusic;
		MusicPlayer.loop = true;
		MusicPlayer.Play();
		SpashScreen.GetComponent<DelayedHide>().Hide(1f);
	}

	IEnumerator PlayGameMusicAndClose()
	{
		yield return new WaitForSeconds(0.3f);
		MusicPlayer.Stop();
		MusicPlayer.clip = GameMusic;
		MusicPlayer.loop = true;
		MusicPlayer.Play();
		InfoBar.SetActive(true);
		KeyboardControlsInfo.SetActive(true);
		KeyboardControlsInfo.GetComponent<DelayedHide>().Hide(0f);
		ScoreManager.Reset();
		ScoreManager.StartTimer();
		Player1Winner.gameObject.SetActive(false);
		Player2Winner.gameObject.SetActive(false);
		gameObject.SetActive(false);
	}

	IEnumerator HideKeyboardControls()
	{
		KeyboardControlsInfo.SetActive(false);
		yield break;
	}

	public void OnStartClicked()
	{
		MusicPlayer.PlayOneShot(SelectSound);
		StartCoroutine(PlayGameMusicAndClose());
		StartCoroutine(HideKeyboardControls());
	}

	public void OnPlayer2Clicked()
	{
		SoundManager.Player2AI = !SoundManager.Player2AI;

		if (SoundManager.Player2AI)
		{
			Player2Button.image.sprite = RobotIcon;
			Player2Line.HumanControlled = false;
			Player2Rod.HumanControlled = false;
			Player2TouchControls.gameObject.SetActive(false);
			Player2Stick.gameObject.SetActive(false);
			Player2KeyboardControls.gameObject.SetActive(false);
		}
		else
		{
			Player2Button.image.sprite = HumanIcon;
			Player2Line.HumanControlled = true;
			Player2Rod.HumanControlled = true;
			Player2TouchControls.gameObject.SetActive(true);
		}
	}

	public void OnGameJoltClicked()
	{
		Application.OpenURL("https://gamejolt.com/games/Just4TheHalibut/360745");
	}

	public void OnYPSClicked()
	{
		Application.OpenURL("http://patreon.com/youreperfect");
	}
}
