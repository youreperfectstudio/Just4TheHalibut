﻿using CnControls;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TouchControlsButton : MonoBehaviour
{

	public bool Enabled;
	public Sprite KeyboardIcon;
	public Sprite TouchscreenIcon;
	public Sprite GamepadIcon;
	public Text KeyboardText;
	public SimpleJoystick Joystick;

	Button AsButton;

	private void Start()
	{
		Enabled = (Input.touchSupported && Application.platform != RuntimePlatform.WebGLPlayer);
		AsButton = GetComponent<Button>();
	}

	public void OnClick()
	{
		Enabled = !Enabled;
	}

	void Update()
    {
		if(Enabled)
		{
			AsButton.image.sprite = TouchscreenIcon;
			Joystick.gameObject.SetActive(true);
			KeyboardText.gameObject.SetActive(false);
		}
		else
		{
			AsButton.image.sprite = KeyboardIcon;
			Joystick.gameObject.SetActive(false);
			KeyboardText.gameObject.SetActive(true);
		}
	}
}
