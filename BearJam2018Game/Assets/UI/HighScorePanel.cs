﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScorePanel : MonoBehaviour
{
	public Button SubmitButton;
	public InputField Initials;

    // Start is called before the first frame update
    void Start()
    {
        
    }

	private void OnEnable()
	{
		Initials.text = PlayerPrefs.GetString("Initials", "");
	}

	// Update is called once per frame
	void Update()
    {
		if (Application.internetReachability == NetworkReachability.NotReachable || Initials.text.Length < 1)
			SubmitButton.gameObject.SetActive(false);
		else
			SubmitButton.gameObject.SetActive(true);

	}

	public void OnCancelClicked()
	{
		gameObject.SetActive(false);
	}
}
