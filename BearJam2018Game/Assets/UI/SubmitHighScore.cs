﻿using GameJolt.API;
using GameJolt.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubmitHighScore : MonoBehaviour
{
	public InputField Initials;
	public HighScorePanel Panel;

    // Start is called before the first frame update
    void Start()
    {
	}

    // Update is called once per frame
    void Update()
    {
    }

	public string NaughtyFilter(string input)
	{
		string[] words = { "fuck","shit","damn","cunt","asshole","clit","cock","dick","crap","piss","bitch","choad","twat","fag" };
		foreach(string word in words)
		{
			if (input.ToLower().Contains(word))
				return "Catherine Irkalla";
		}
		return input;
	}

	public void OnClick()
	{
		int score = PlayerPrefs.GetInt("HighScore", 0);
		string name = NaughtyFilter(Initials.text);
		if (score != 0)
		{
			GameJolt.API.Scores.Add(score, score + " Points", name);
			PlayerPrefs.SetString("Initials", name);
			StartCoroutine(OnClickHelper(1f));
		}
	}

	public IEnumerator OnClickHelper(float delay)
	{
		yield return new WaitForSeconds(delay);
		GameJoltUI.Instance.ShowLeaderboards();
		Panel.gameObject.SetActive(false);
	}
}
