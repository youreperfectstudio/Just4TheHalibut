﻿using GameJolt.API;
using GameJolt.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerSignIn : MonoBehaviour
{
	public Sprite DefaultIcon;

	public void OnClick()
	{
		if(GameJoltAPI.Instance.HasSignedInUser)
		{
			GameJoltAPI.Instance.CurrentUser.SignOut();
			GetComponent<Button>().image.sprite = DefaultIcon;
		}
		else
		{
			GameJoltUI.Instance.ShowSignIn(null, Status);
		}
	}

	public void Status(bool success)
	{
		if (success)
			GameJoltAPI.Instance.CurrentUser.DownloadAvatar(Downloaded);
		else
			GetComponent<Button>().image.sprite = DefaultIcon;
	}

	public void Downloaded(bool success)
	{
		Button asButton = GetComponent<Button>();
		if (success)
			asButton.image.sprite = GameJoltAPI.Instance.CurrentUser.Avatar;
		else
			asButton.image.sprite = DefaultIcon;
	}
}
