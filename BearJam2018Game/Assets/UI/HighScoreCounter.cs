﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HighScoreCounter : MonoBehaviour
{
	private void OnEnable()
	{
		GetComponent<Text>().text = PlayerPrefs.GetInt("HighScore", 0).ToString();
	}

	public void OnClear()
	{
		PlayerPrefs.SetInt("HighScore", 0);
		GetComponent<Text>().text = "0";
	}
}
