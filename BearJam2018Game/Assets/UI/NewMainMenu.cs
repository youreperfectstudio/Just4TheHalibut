﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NewMainMenu : MonoBehaviour
{

	public LineController Player2Line;
	public RodController Player2Rod;
	public Image SpashScreen;
	public SpriteRenderer Player2Sprite;
	public Animator Player2Animator;
	public Sprite BearSprite;
	public Sprite RobotSprite;
	public RuntimeAnimatorController BearAnimator;
	public RuntimeAnimatorController RobotAnimator;


	private void Start()
	{
		SpashScreen.gameObject.SetActive(true);
		SpashScreen.GetComponent<DelayedHide>().Hide(1.2f);
	}

	public void On1PlayerClicked()
	{
		SoundManager.Player2AI = true;
		Player2Line.HumanControlled = false;
		Player2Rod.HumanControlled = false;
		gameObject.SetActive(false);
		Player2Sprite.sprite = RobotSprite;
		Player2Animator.runtimeAnimatorController = RobotAnimator;
	}

	public void On2PlayerClicked()
	{
		SoundManager.Player2AI = false;
		Player2Line.HumanControlled = true;
		Player2Rod.HumanControlled = true;
		gameObject.SetActive(false);
		Player2Sprite.sprite = BearSprite;
		Player2Animator.runtimeAnimatorController = BearAnimator;
	}
}
