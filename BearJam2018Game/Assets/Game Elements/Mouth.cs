﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Mouth : MonoBehaviour
{
	public GameObject colliders;
	public GameObject pos;
	public SpriteRenderer[] sprite;
	public Text p1_score;
	public Text p2_score;
	public int player;

	bool hooked = false;
	void Start()
	{
		colliders.SetActive(false);

		sprite = GetComponentsInChildren<SpriteRenderer>();
		foreach (SpriteRenderer rend in sprite)
		{
			rend.enabled = false;
		}
	}

	void Update()
	{
		if (hooked)
		{
			transform.position = pos.transform.position;
		}
	}

	public void EnableHooked()
	{
		string str = "rod" + player;
		Debug.Log(str);
		GameObject rodObj = GameObject.Find(str);

		if (player == 1)
		{
			SwingController lc = rodObj.GetComponent<SwingController>();
			lc.hooked = true;
			lc.hookedFish = transform.gameObject;
			string hookstr = "hook" + player;
			Debug.Log(hookstr);
			pos = GameObject.Find(hookstr);
			pos.GetComponent<BoxCollider2D>().enabled = false;
		}
		else
		{
			SwingController lc = rodObj.GetComponent<SwingController>();
			lc.hooked = true;
			lc.hookedFish = transform.gameObject;
			string hookstr = "hook" + player;
			Debug.Log(hookstr);
			pos = GameObject.Find(hookstr);
			pos.GetComponent<BoxCollider2D>().enabled = false;
			AIbear ai = GameObject.Find("AI").GetComponent<AIbear>();
			ai.hooked = true;
		}
		hooked = true;
	}



	public void DestroyFish()
	{
		string str = "rod" + player;
		GameObject rodObj = GameObject.Find(str);
		if (player == 1)
		{
			SwingController lc = rodObj.GetComponent<SwingController>();
			lc.hooked = false;
		}
		else
		{
			SwingController lc = rodObj.GetComponent<SwingController>();
			lc.hooked = false;
			if (GameObject.Find("AI") != null)
			{
				AIbear ai = GameObject.Find("AI").GetComponent<AIbear>();
				ai.target = null;
				ai.hooked = false;
			}
		}
		pos.GetComponent<BoxCollider2D>().enabled = true;
		Destroy(transform.parent.gameObject);

	}
	public void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.tag == "hook1")
		{
			hooked = true;
			foreach (SpriteRenderer rend in sprite)
			{
				rend.enabled = true;
			}
			//disable fish obj and its script
			Fish fishScript = transform.parent.GetComponent("Fish") as Fish;
			fishScript.enabled = false;
			SpriteRenderer parentspr = transform.parent.GetComponent("SpriteRenderer") as SpriteRenderer;
			parentspr.enabled = false;
			player = 1;
			EnableHooked();




		}

		if (coll.tag == "hook2")
		{
			hooked = true;
			foreach (SpriteRenderer rend in sprite)
			{
				rend.enabled = true;
			}
			//disable fish obj and its script
			Fish fishScript = transform.parent.GetComponent("Fish") as Fish;
			fishScript.enabled = false;
			SpriteRenderer parentspr = transform.parent.GetComponent("SpriteRenderer") as SpriteRenderer;
			parentspr.enabled = false;
			player = 2;
			EnableHooked();
		}

		if (coll.tag == "surfacelayer")
		{
			Fish fish = transform.parent.GetComponent("Fish") as Fish;
			if (player == 1)
				ScoreManager.Player1Score += fish.PointValue;
			else
				ScoreManager.Player2Score += fish.PointValue;

			if (fish.PointValue >= 4)
				SoundManager.PlayBigScore();
			else
				SoundManager.PlayScore();

			GameObject playerObject = (player == 1 ? SoundManager.Instance.LeftBear.gameObject : SoundManager.Instance.RightBear.gameObject);

			//StartCoroutine(playerObject.GetComponent<Bear>().PlayScoreAnimation(fish.PointValue));
			DestroyFish();

		}

		if (coll.tag == "whale")
		{
			//If whale eats fish, increase timer
			coll.GetComponent<Whale>().PlayEatAnimation();
			Fish fish = transform.parent.GetComponent("Fish") as Fish;
			ScoreManager.TimerTime += (fish.PointValue * 2);
			SoundManager.PlayWhale();
			DestroyFish();
		}
	}
}
