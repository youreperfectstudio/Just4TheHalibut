﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Pond : MonoBehaviour {

	//Old version of population stuff is below, you can delete it if using another method

	public Fish FishPrefab;
	public List<FishType> PossibleFish;
	public int MinimumFishCount = 10;

	private List<Fish> SpawnedFish = new List<Fish>();
	//Cheap and easy way to do this
	private int[] ProbabilityHelper = { 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5 };


	private void Update()
	{
		

	}

	public void SpawnRandomFish(int depth)
	{
		//Get list of fish that are
		List<FishType> fishAtDepth = new List<FishType>();
		foreach (FishType fishType in PossibleFish)
		{
			if (fishType.PreferredDepth == depth ||
				fishType.PreferredDepth - 1 == depth ||
				fishType.PreferredDepth + 1 == depth)
				fishAtDepth.Add(fishType);
		}

		int rarity = ProbabilityHelper[Random.Range(0, ProbabilityHelper.Length)];

		//TODO this can be done once and cached
		List<FishType> fishWithRarity = new List<FishType>();
		foreach (FishType fishType in fishAtDepth)
		{
			if (fishType.Rarity == rarity)
				fishWithRarity.Add(fishType);
		}

		FishType slectedType = fishWithRarity[Random.Range(0, fishWithRarity.Count)];

		Fish fish = Instantiate(FishPrefab) as Fish;
	}
}
