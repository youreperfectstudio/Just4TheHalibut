﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bear : MonoBehaviour {

	/// <summary>
	/// This currently doesn't work...
	/// </summary>
	/// <param name="points"></param>
	/// <returns></returns>
	public IEnumerator PlayScoreAnimation(int points)
	{
		Animator anim = GetComponent<Animator>();
		anim.SetBool("score", true);
		yield return new WaitForSeconds(points);
		anim.SetBool("score", false);
	}
}
