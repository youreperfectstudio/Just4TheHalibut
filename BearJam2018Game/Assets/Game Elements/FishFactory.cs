﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using System.Collections;
using UnityEngine;

public class FishFactory : MonoBehaviour
{

	public int MinimumFishCount;
	public GameObject[] fishPrefabs;
	public GameObject[] veryRareFish;
	public LayerMask FishLayer;

	public float veryRareFishProbability = 0.001f;
	public float delay = 60.0f * 60.0f;
	public bool active = true;
	public float rangeVal = 4;
	public Vector2 delayRange = new Vector2(1, 2);
	private static FishFactory Instance;

	public static void SpawnFish(int howMany)
	{
		while (howMany-- > 0)
		{
			Vector3 newVect = new Vector3(Instance.transform.position.x, Instance.transform.position.y + Random.Range(-Instance.rangeVal, Instance.rangeVal), Instance.transform.position.z);

			GameObject newFish;
			if (Random.value < Instance.veryRareFishProbability && Instance.veryRareFish.Length > 0)
				newFish = Instantiate(Instance.veryRareFish[Random.Range(0, (Instance.veryRareFish.Length) - 1)], newVect, Quaternion.identity, Instance.transform);
			else
				newFish = Instantiate(Instance.fishPrefabs[Random.Range(0, (Instance.fishPrefabs.Length) - 1)], newVect, Quaternion.identity, Instance.transform);
		}
	}

	public static void MinimumFishCheck()
	{
		GameObject[] fish = GameObject.FindGameObjectsWithTag("fish");
		if (fish.Length < Instance.MinimumFishCount)
			SpawnFish(1);
	}

	void Awake()
	{
		Instance = this;
	}

	void Start()
	{
		StartCoroutine(FishGenerator());
	}

	void Update()
	{
		MinimumFishCheck();
	}
	IEnumerator FishGenerator()
	{
		yield return new WaitForSeconds(delay);

		if (active)
		{
			ResetDelay();
			SpawnFish(1);
			ResetDelay();
		}
		StartCoroutine(FishGenerator());
	}
	void ResetDelay()
	{
		delay = Random.Range(delayRange.x, delayRange.y);
	}
}
