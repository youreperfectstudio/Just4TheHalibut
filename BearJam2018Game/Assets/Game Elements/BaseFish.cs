﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using UnityEngine;

public class BaseFish : MonoBehaviour
{

	public float Duration;    //the max time of a move session
	float ElapsedTime = 0f; //time since started moving
	float Wait = 0f; //wait this much time
	float WaitTime = 0f; //waited this much time
	public int Cycle = 1;
	public float Seed = 1;
	public bool Move = false;
	public Rigidbody2D Rigidbody;

	void Start()
	{
		Rigidbody = GetComponent<Rigidbody2D>();
	}

	void Update()
	{
		if (ElapsedTime < Duration && Move)
		{
			Rigidbody.velocity = transform.right * Seed;
			ElapsedTime += Time.deltaTime;
		}
		else
		{
			Cycle = (Cycle + 1) % 2;
			transform.Rotate(0, 180, 0);
			Duration = Random.Range(1, 30);
			ElapsedTime = 0;
			Move = true;
		}
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.tag == "boundary")
		{
			Cycle = (Cycle + 1) % 2;
			transform.Rotate(0, 180, 0);
			Duration = Random.Range(1, 30);
			ElapsedTime = 0;
			Move = true;
		}
	}
}
