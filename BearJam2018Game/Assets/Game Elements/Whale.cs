﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Whale : MonoBehaviour
{
	public float speed = 1;
	public Rigidbody2D Rigidbody;

	void Start()
	{
		Rigidbody = GetComponent<Rigidbody2D>();
		Rigidbody.velocity = -transform.right * speed;
	}

	public void OnTriggerEnter2D(Collider2D coll)
	{
		if (coll.tag == "boundary")
		{
			StartCoroutine(PlayTurnAnimation());
		}
	}

	private IEnumerator PlayTurnAnimation()
	{
		GetComponent<Animator>().SetBool("turning", true);
		yield return new WaitForSeconds(0.2f);
		GetComponent<Animator>().SetBool("turning", false);
		transform.Rotate(0, 180, 0);
		speed = Random.Range(3, 7);
		Rigidbody.velocity = -transform.right * speed;
	}

	public void PlayEatAnimation()
	{
		GetComponent<Animator>().SetBool("eating", true);
		StartCoroutine(PlayEatAnimationHelper());
	}

	private IEnumerator PlayEatAnimationHelper()
	{
		yield return new WaitForSeconds(0.25f);
		GetComponent<Animator>().SetBool("eating", false);
	}
}
