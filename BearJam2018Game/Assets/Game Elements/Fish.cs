﻿/*  
 * Copyright (C) 2018 You're Perfect Studio. All rights reserved.  
 * Licensed under the BSD 3-Clause License, the MIT License, and/or the Creative Commons 0 License
 * Your choice which of these three licenses to use.
 * See LICENSE files in the project root for full license information.
 * See CONTRIBUTORS file in the project root for full list of contributors.
 */
using UnityEngine;

public class Fish : BaseFish {

	public string FishName = "Fish";


	[Tooltip("0-10")]
	public int PointValue = 1;

	[Tooltip("1-5. 1 is common, 5 is rare")]
	public int Rarity = 1;

	[Tooltip("Related to size of fish. 0 means it doesn't help hunger. 100 means bear is no longer hungry.")]
	public int HungerSatiation = 1;

	[Tooltip("1 is preferres shallow, 10 is preffers deep")]
	public int PreferredDepth = 0;
}
