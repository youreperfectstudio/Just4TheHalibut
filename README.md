# Just for the Halibut!
[Play or download Just for the Halibut! here](https://gamejolt.com/games/Just4TheHalibut/360745)

Beary Offishial Really Fun Game Just for the Halibut! is our entry into the [2018 Game Jolt Bear Jam.](http://jams.gamejolt.io/bearweekjam2018)  -- #BearJam2018

Collect as many fish as you can before time runs out! 
Watch out for the whale who wants to eat your fish!
If the whale eats fish, the timer increases.

Play against a friend or the computer.
Click the robot button to have a human player 2.

Supports XBox 360 controllers on Windows.

Android version coming soon!

Special thanks to [Void](gitlab.com/Voyd) for helping program most of the game, [mepavi ](https://www.instagram.com/mepavi4711/) for doing the pixel art, memoraphile for the music and sound effects, and [Matt](https://twitter.com/mattisverytoxic) for getting the project re-started after having been cancelled! <3 <3 <3

This game is inspired by the Atari 2600 Classic [_Fishing_ _Derby_](https://en.wikipedia.org/wiki/Fishing_Derby) by Activision.

# Building this game from source code

See our [Wiki pages](https://gitlab.com/youreperfectstudio/Just4TheHalibut/wikis/@welcome) on how to download and build the project.

# Licensing

All original source code and assets are tripple BSD/MIT/CC0 licensed. Use whicever one you want. Most original assets are also posted [on our OpenGameArt page](https://opengameart.org/users/youre-perfect-studio)